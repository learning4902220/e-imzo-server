package com.example.demo.eImzoServer.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Pkcs7WithTimestamp extends JsonResponse {

    private String pkcs7b64;
    private TimestampedSignerList[] timestampedSignerList;

}
