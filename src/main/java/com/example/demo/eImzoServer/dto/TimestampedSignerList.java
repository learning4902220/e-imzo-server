package com.example.demo.eImzoServer.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TimestampedSignerList {

    private String serialNumber;
    private Map<String, String> subjectInfo;
    private String validFrom;
    private String validTo;
}
