package com.example.demo.eImzoServer.dto;

import java.util.LinkedList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Pkcs7InfoView {

    private List<Pkcs7SignerInfoView> signers = new LinkedList<>();
    private String documentBase64;
}
