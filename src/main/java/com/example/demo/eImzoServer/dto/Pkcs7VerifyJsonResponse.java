package com.example.demo.eImzoServer.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Pkcs7VerifyJsonResponse extends JsonResponse {

    private Pkcs7InfoView pkcs7Info;
}
