package com.example.demo.eImzoServer.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ESKApiResponse {

    private int code;
    private String message;
    private Pkcs7InfoView pkcs7InfoView;
    private String pkcs7b64WithTimestamp;
}
