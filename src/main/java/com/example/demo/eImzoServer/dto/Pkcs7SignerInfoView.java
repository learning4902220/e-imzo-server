package com.example.demo.eImzoServer.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Pkcs7SignerInfoView {

    private SignerIdView signerId;
    private String signingTime;
    private String signature;
    private String digest;
    private TimeStampInfoView timeStampInfo;
    private CertificateView[] certificate;
    private String OCSPResponse;
    private String statusUpdatedAt;
    private String statusNextUpdateAt;
    private boolean verified;
    private boolean certificateVerified;
    private CertificateView trustedCertificate;
    private List<String> policyIdentifiers;
    private boolean certificateValidAtSigningTime;
    private RevokedStatusInfoView revokedStatusInfo;
    private String exception;
}
