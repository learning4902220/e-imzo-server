package com.example.demo.eImzoServer.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CertificateView {

    private String serialNumber;
    private String subjectName;
    private Map<String, String> subjectInfo;
    private String validFrom;
    private String validTo;
    private String issuerName;
    private Map<String, String> issuerInfo;
    private PublicKeyInfoView publicKey;
    private SignatureInfoView signature;
}
