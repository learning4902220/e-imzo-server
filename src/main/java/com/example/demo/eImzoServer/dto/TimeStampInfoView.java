package com.example.demo.eImzoServer.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TimeStampInfoView {

    private CertificateView[] certificate;
    private String OCSPResponse;
    private String statusUpdatedAt;
    private String statusNextUpdateAt;
    private boolean digestVerified;
    private boolean certificateVerified;
    private CertificateView trustedCertificate;
    private boolean certificateValidAtSigningTime;
    private RevokedStatusInfoView revokedStatusInfo;
}
