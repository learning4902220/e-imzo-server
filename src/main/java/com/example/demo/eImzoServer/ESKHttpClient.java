package com.example.demo.eImzoServer;

import com.example.demo.eImzoServer.dto.ESKApiResponse;
import com.example.demo.eImzoServer.dto.Pkcs7VerifyJsonResponse;
import com.example.demo.eImzoServer.dto.Pkcs7WithTimestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.InetSocketAddress;
import java.net.Proxy;

public class ESKHttpClient {
    private final static Logger log = LoggerFactory.getLogger(ESKHttpClient.class);
    private final String eImzoHost;
    private final RestTemplate restTemplate;

    public ESKHttpClient(String eImzoHost, int connectionTimeout, int readTimeout, boolean useProxy) {
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setReadTimeout(readTimeout);
        requestFactory.setConnectTimeout(connectionTimeout);
        if (useProxy) {
            Proxy proxy = new Proxy(java.net.Proxy.Type.HTTP, new InetSocketAddress("tk-wsa.bee.unitel.local", 3128));
            requestFactory.setProxy(proxy);
        }
        this.restTemplate = new RestTemplate(requestFactory);
        this.eImzoHost = eImzoHost;
    }

    public ESKHttpClient(String eImzoHost, boolean useProxy) {
        this(eImzoHost, 5000, 30000, useProxy);
    }

    public String attachTimestampToPkcs7(String userRealIP, String host, String pkcs7b64) {
        log.info("Attaching Timestamp to PKCS7 document method with params of userRealIP: {}, Host: {}, pkcs6b64 length: {}", userRealIP, host, pkcs7b64.length());
        String fullUrl = eImzoHost + "/frontend/timestamp/pkcs7";
        byte[] pkcs7b64Bytes = pkcs7b64.getBytes();
        HttpHeaders headers = makeHeaders(userRealIP, host, (long) pkcs7b64Bytes.length);
        HttpEntity<?> request = new HttpEntity<>(pkcs7b64Bytes, headers);
        ESKApiResponse eskApiResponse = post(fullUrl, request, false);
        log.info("Response of attachTimestamp is: -------> {}", eskApiResponse.getMessage().toUpperCase());
        return eskApiResponse.getPkcs7b64WithTimestamp();
    }

    public ESKApiResponse verifyPkcs7Attached(String userRealIP, String host, String pkcs7b64) {
        log.info("Verify Pkcs7Attached method with params of userRealIP: {}, Host: {}, pkcs6b64 length: {}", userRealIP, host, pkcs7b64.length());
        String fullUrl = eImzoHost + "/backend/pkcs7/verify/attached";
        byte[] pkcs7b64Bytes = pkcs7b64.getBytes();
        HttpHeaders headers = makeHeaders(userRealIP, host, (long) pkcs7b64Bytes.length);
        HttpEntity<?> request = new HttpEntity<>(pkcs7b64Bytes, headers);
        ESKApiResponse eskApiResponse = post(fullUrl, request, true);
        log.info("Response of verify pkcs7 is: -------> {}", eskApiResponse.getMessage().toUpperCase());
        return eskApiResponse;
    }

    private HttpHeaders makeHeaders(String userRealIP, String host, Long contentLength) {
        log.info("Make Headers of X-Real-IP: {}, Host: {}, X-Real-Host: {}, ContentLength: {}, ContentType: {}",
                userRealIP, host, host, contentLength, MediaType.APPLICATION_OCTET_STREAM);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentLength(contentLength);
        headers.set("X-Real-IP", userRealIP);
        headers.set("Host", host);
        headers.set("X-Real-Host", host);
        return headers;
    }

    private ESKApiResponse post(String url, HttpEntity<?> request, boolean isAttachTimestampByFront) {
        log.info("Send POST request to verify pkcs7 by external endpoint: {}", url);
        ESKApiResponse result = new ESKApiResponse();
        try {
            if (!isAttachTimestampByFront) {
                Pkcs7WithTimestamp pkcs7WithTimestamp = restTemplate.postForObject(url, request, Pkcs7WithTimestamp.class);
                if (pkcs7WithTimestamp != null) {
                    log.info("Response of attach timestamp to pkcs7 document. STATUS: {}.", pkcs7WithTimestamp.getStatus());
                    result.setCode(pkcs7WithTimestamp.getStatus());
                    result.setMessage(pkcs7WithTimestamp.getStatus() == 1 ? "Успешно" : getStatusInfo(pkcs7WithTimestamp.getStatus()));
                    result.setPkcs7b64WithTimestamp(pkcs7WithTimestamp.getPkcs7b64());
                    return result;
                } else {
                    log.info("Error-response for post request: '/frontend/timestamp/pkcs7' with STATUS: {}.", pkcs7WithTimestamp.getStatus());
                    throw new HttpClientErrorException(HttpStatus.BAD_GATEWAY, "Response of PKCS7-Info is NULL! ");
                }
            } else {
                Pkcs7VerifyJsonResponse response = restTemplate.postForObject(url, request, Pkcs7VerifyJsonResponse.class);
                if (response != null) {
                    log.info("Response of verify pkcs7 document. STATUS: {}.", response.getStatus());
                    result.setCode(response.getStatus());
                    result.setMessage(response.getStatus() == 1 ? "Успешно" : getStatusInfo(response.getStatus()));
                    result.setPkcs7InfoView(response.getPkcs7Info());
                    return result;
                } else {
                    log.info("Error-response for post request: '/backend/pkcs7/verify/attached' with STATUS: {}.", response.getStatus());
                    throw new HttpClientErrorException(HttpStatus.BAD_GATEWAY, "Response of PKCS7-Info is NULL! ");
                }
            }
        } catch (HttpClientErrorException exception) {
            log.error("Error-response for post request: {} with cause: {}", url, exception.getMessage());
            result.setCode(exception.getStatusCode().value());
            result.setMessage(exception.getMessage());
            return result;
        }
    }

    private String getStatusInfo(int status) {
        switch (status) {
            case 1:
                return "Успешно";
            case -1:
                return "Неудалось проверить статус сертификата. URL-адрес ocsp запрещен";
            case -10:
                return "ЭЦП недействительна";
            case -11:
                return "Сертификат недействителен";
            case -12:
                return "Сертификат недействителен на дату подписи";
            case -20:
                return "Неудалось проверить статус сертификата Timestamp";
            case -21:
                return "ЭЦП или хеш Timestamp недействительна";
            case -22:
                return "Сертификат Timestamp недействителен";
            case -23:
                return "Сертификат Timestamp недействителен на дату подписи";
            default:
                return "Неизвестная ошибка не входящая в нормы запроса!";
        }
    }
}
