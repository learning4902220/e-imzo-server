package com.example.demo.eImzoServer;

import com.example.demo.eImzoServer.dto.ESKApiResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;

@Component
public class ESKComponent {
    private final String ourDomain;
    private final String serverIP;
    private final ESKHttpClient client;

    public ESKComponent(@Value("${imzo.e-imzo-server-host}") String eImzoHost, @Value("${imzo.our-domain}") String ourDomain,
                        @Value("${imzo.useProxy}") boolean useProxy) {
        this.ourDomain = ourDomain;
        this.client = new ESKHttpClient(eImzoHost, useProxy);
        this.serverIP = getServerIp(eImzoHost);
    }

    private String getServerIp(String eImzoHost) {
        String serverIP = "0.0.0.0";
        try {
            serverIP = new URI(eImzoHost).getHost();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return serverIP;
    }

    public String attachTimestampToPkcs7(String pkcs7b64) {
        return client.attachTimestampToPkcs7(serverIP, ourDomain, pkcs7b64);
    }

    public ESKApiResponse verify(String pkcs7b64) {
        return client.verifyPkcs7Attached(serverIP, ourDomain, pkcs7b64);
    }
}
